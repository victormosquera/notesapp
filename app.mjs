import express from 'express';
import path from 'path';
import handlebars from 'express-handlebars';
const __dirname = path.dirname(new URL(import.meta.url).pathname);
import methodOverride from 'method-override';
import session from 'express-session';
import sessionFileStore from 'session-file-store';
import morgan from 'morgan';
import { router as index } from './routes/index.mjs';
import { router as users} from './routes/users';
import { router as notes } from './routes/notes';
import {initPassport} from './config/passport';
import {mongoConn} from './config/db';
import flash from 'connect-flash';
import cookieParser from 'cookie-parser';
const FileStore = sessionFileStore(session);
const part = new FileStore() 
export const sessionCookieName = 'notescookie.sid';

//---------init....--------
const app = express()
mongoConn() 

//---------------static files-------------------
app.use(express.static(path.join(__dirname, 'public')));


//---------------settings-------------------
app.set('port', process.env.PORT || 3000);
app.engine('.hbs', handlebars({
    defaultLayout:'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs'
}) ); 
app.set('view engine', '.hbs');

//---------------midlewares-------------------
app.use(cookieParser());
app.use(express.urlencoded({extended: false})) // para poder recibir datos de formmularios 
app.use(methodOverride('_method'))
app.use(session({
    store: part,
    secret: 'keyboard cat',
    resave:false,
    saveUninitialized:true,
    name :sessionCookieName
}));
initPassport(app)
app.use(flash())  
app.use(morgan('dev'))
//---------------Globas Vars-------------------
app.use( (req, res, next) => {
    res.locals.success_msg = req.flash('success_msg')
    res.locals.error_msg = req.flash('error_msg')
    res.locals.error = req.flash('error')
    res.locals.user = req.user || null
    next()
});

//---------------rotutes-------------------

app.use('/', index)
app.use('/users', users)
app.use('/notes', notes)


//---------------Server Listen-------------------
app.listen(app.get('port'), () => console.log('App listening on port'+app.get('port')) );

export default app