import passport from 'passport';
import passportlocal from 'passport-local';
const localStrategy = passportlocal.Strategy
import userModel from '../models/user';


export function initPassport(app) { 
    app.use(passport.initialize()); 
    app.use(passport.session()); 
}

passport.use( new localStrategy({
    usernameField: 'email',

},async (email, password, done) => {
    try {
        const user = await userModel.findOne({email: email})
        if (!user) {
            return done(null, false, {message: 'User not Found!'})
        }
        const passCompare = await user.comparePassword(password)
        if (!passCompare) {
            return done(null, false, {message: 'Your password is incorrect'})
        }        
        return done(null, user)
        
    } catch (error) {
        console.log(error);
    }
    
}))


passport.serializeUser((user, done) => {// serializa los datos para guardarlo en una session
    done(null, user.id)
})

passport.deserializeUser(async (id, done) => {
    try {
        const User = await userModel.findById(id)
        done(null, User) 
    } catch (error) {
        console.log(error);        
    }
    
})