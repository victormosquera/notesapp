import mongoose from 'mongoose';
import key from './key';

export function mongoConn() {
    mongoose.connect(key, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false
}).then(db => console.log('Db is conected atlas'))
.catch(err => console.log(err))

}

