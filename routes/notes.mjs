import express from 'express';
export const router = express.Router()
import modelNote from '../models/note';
import { log } from 'util';
import {isAuthenticated} from './users.mjs';

router.get('/',isAuthenticated, async (req, res, next) => {
    try {
        const notes = await modelNote.find({user: req.user.id}).sort({date:'desc'})
        res.render('notes/allnotes',{notes})
    } catch (error) {
        console.log(error);   
    }
})


router.get('/addnote', isAuthenticated , (req, res, next) => {
    res.render('notes/addnote')
})

router.post('/addnote', async (req, res, next) => {
    const {title, description } = req.body
    const errors = []
    try {
        if (!title) {
            errors.push({text: 'Please write a title'})
        }
        if (!description) {
            errors.push({text: 'Please write an descrition task'})
        }
        if (errors.length>0) {
            res.render('newnote' , {
                errors,
                title,
                description 
            })
        }else{
            const newNote = new modelNote({title, description})
            newNote.user = req.user.id
            await newNote.save()
            req.flash('success_msg', 'Note Added Successfully')
            res.redirect('/notes')  
        } 
    } catch (error) {
        console.log(error);        
    }
    
})


router.get('/editnote/:id',isAuthenticated, async (req, res, next) => {
    try {
        const note = await modelNote.findById(req.params.id)
        res.render('notes/editnote', {note})
    } catch (error) {
        console.log(error);
    }
})

router.put('/editnote/:id', async (req, res, next) => {
    const {title, description}= req.body
    try {
        await modelNote.findByIdAndUpdate(req.params.id, {title, description})
        req.flash('success_msg', 'Note update Successfully')
        res.redirect('/notes')
    } catch (error) {
        console.log(error);
    } 
})

router.get('/deletenote/:id',isAuthenticated, async (req, res, next) => {
    try {
        await modelNote.findByIdAndDelete(req.params.id)
        req.flash('success_msg', 'Note delete Successfully')
        res.redirect('/notes')
    } catch (error) {
        console.log(error);
    }
})

