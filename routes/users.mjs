import express from 'express';
export const router = express.Router()
import userModel from '../models/user';
import passport from 'passport';
import { sessionCookieName } from '../app';



router.get('/login', (req, res, next) => {
    res.render('auth/login')
})

router.get('/register', (req, res, next) => {
    res.render('auth/register')
})

router.post('/register', async (req, res, next) => {
    const {name, email, password, password2} = req.body
    const errors = []
    if (password != password2) {
        errors.push({text:'password do not match'})
    }
    if (password.length < 6) {
        errors.push({text:'The password must have a minimum of 6 characters'})
    }
    if (errors.length>0) {
        res.render('auth/register', {errors, name, email, password, password2})
    }else{
        const userfind = await userModel.findOne({email :email})
        if (userfind) {
            req.flash('success_msg', 'this email is registered')
            res.redirect('/users/register')
        }
        const newUser = new userModel({name, email, password})
        newUser.password = await newUser.encryptPassword(password)
        await newUser.save()
        req.flash('success_msg', 'you are registered')
        res.redirect('/users/login')
    }

})

export function isAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next()
    }else{
        req.flash('error_msg', 'Not Authorized')
        res.redirect('/users/login')
    }
}

router.get('/logout', (req, res, next) => {
        req.session.destroy();//borra las credenciales de inicio de sesion
        req.logout(); 
        res.clearCookie(sessionCookieName);//borra datos de la cookie
        res.redirect('/');
    //res.clearCookie(sessionCookieName);//borra datos de la cookie
})


router.post('/login', passport.authenticate('local',{
    successRedirect:'/notes',
    failureRedirect:'/users/login',
    failureFlash: true
}))

