import mongoose from 'mongoose';
const Schema =  mongoose.Schema

const notesSchema = new Schema({
    title:{
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    date:{
        type: Date,
        default: Date.now
    },
    user:{
        type:String
    }
})
const modelNote = mongoose.model('note', notesSchema)

export default modelNote